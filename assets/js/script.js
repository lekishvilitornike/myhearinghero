window.onload = function () {
    var anchors = document.querySelectorAll("a[href^='order']");
    var param = window.location.search.substring(1);
    const url = 'https://example.com?' + param;
    for (var i = 0; i < anchors.length; i++) {
        anchors[i].href = url;
    }
};
